package com.example.cse438.studio2.model

class SiteDetail(
    private var url: String,
    private var name: String,
    private var sku: String,
    private var recentoffers_count: Int,
    private var latestoffers: ArrayList<Offer>
)